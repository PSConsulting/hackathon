<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <script src="http://cdn.traitify.com/js/api/1.0.0.js"></script>
    <script src="http://cdn.traitify.com/js/widgets/slide_deck/1.0.0.js"></script>
    <script>var assessmentId = "e82ce555-7db3-40cb-89a7-72258e5cbdc0";</script>
</head>
<body>

    <div class="tf-assessment" style="width: 500px;"></div>

    <div class="output"></div>

    <script>
        Traitify.setPublicKey("4r90k3jufseqaclg6n21bkgl2m");
        Traitify.setHost("https://api-sandbox.traitify.com");
        Traitify.setVersion("v1");

        Traitify.ui.slideDeck(assessmentId, ".tf-assessment", function(data) {
            Traitify.ui.resultsProp(assessmentId, ".output", {showTraits: true});
        });


    </script>

</body>
</html>
