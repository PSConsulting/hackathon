<style>
    .show-results {
        width: 700px;
    }
    .show-results .badge {
        background-color: transparent;
        background-position: center center;
        background-size: 4em 4em;
        border: 0.2em solid #fff;
        border-radius: 50%;
        display: inline-block;
        font-size: 1em;
        height: 4em;
        margin: -1.4em -0.8em 0;
        padding: 0;
        position: relative;
        vertical-align: middle;
        width: 4em;
        z-index: 1;
    }
    .show-results .positive-bar {
        background-color: #e0e0e0;
        border-radius: 0 0.3em 0.3em 0;
        box-shadow: 0.1em 0.1em 0.2em 0.01em #ccc inset;
        display: inline-block;
        height: 1em;
        overflow: hidden;
        width: 12em;
    }
    .show-results .negative-bar {
        background-color: #e0e0e0;
        border-radius: 0.3em 0 0 0.3em;
        box-shadow: 0.1em 0.1em 0.2em 0.01em #ccc inset;
        display: inline-block;
        height: 1em;
        overflow: hidden;
        width: 12em;
    }
    .show-results .personality {
        display: inline-block;
        padding: 0.3em 0;

    }
    .show-results .prop-results {
        background-color: #fff;
        border-radius: 0.5em;
        display: inline-block;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        margin: 0 auto;
        padding: 1em;
        width: 29.5em;
    }
    .show-results {
        text-align: center;
    }
    .show-results .negative-bar .inner {
        background-color: #e54435;
        height: 1em;
    }
    .show-results .positive-bar .inner {
        background-color: #0f9bd8;
        height: 1em;
    }
    .show-results .labels {
        clear: both;
    }
    .show-results .not-me-label {
        float: left;
        margin-left: 0.9em;
    }
    .show-results .me-label {
        float: right;
        margin-right: 0.9em;
    }
    .show-results .score {
        float: right;
        margin-bottom: -0.5em;
        margin-top: -0.1em;
    }
    .show-results .score.me {
        color: #0f9bd8;
    }
    .show-results .score.not-me {
        color: #e54435;
    }
    .show-results .name {
        float: left;
        margin-bottom: -0.5em;
        margin-top: -0.1em;
    }
    .show-results .personality-traits .name {
        line-height: 1em;
        margin-bottom: 0;
        margin-left: 3em;
        margin-top: 0.5em;
    }
    .show-results .personality-traits .score {
        line-height: 1em;
        margin-bottom: 0;
        margin-right: 3em;
        margin-top: 0.5em;
    }
    .show-results .personality-traits .name-and-score {
        display: block;
    }
    .show-results .personality-traits .bars {
        display: inline-block;
        line-height: 0.3em;
        position: relative;
        top: -0.4em;
    }
    .show-results .personality-traits .negative-bar {
        height: 0.3em;
        line-height: 0.3em;
        width: 10em;
    }
    .show-results .clear {
        clear: both;
    }
    .show-results .personality-traits .positive-bar {
        height: 0.3em;
        line-height: 0.3em;
        width: 10em;
    }
    .show-results .personality-traits .positive-bar .inner {
        height: 0.3em;
    }
    .show-results .personality-traits .negative-bar .inner {
        height: 0.3em;
    }
    .show-results .personality-traits {
        overflow: hidden;
    }

</style>

<?php

//$categories = array(
//    "Visionary" => 0,
//    "Mentor" => -50,
//    "Inventor" => 50,
//    "Action-Taker" => 0,
//    "Naturalist" => 0,
//    "Planner" => 0,
//    "Analayzer" => 0
//);
	#die(var_dump($_POST['avgCats']));
$categories = $_POST['avgCats'];
?>

<div class="show-results">

    <?php foreach($categories as $cat=>$val): ?>
	    <?php $score = $val['score'];
	    $name = ($val['personality_type']['name'] == "Analyzer" ? "Analayzer" : $val['personality_type']['name']);
	    $name = str_replace(' ','-',$name);
	    ?>
        <div class="personality">
            <div class="name"> <?=$name;?> </div>
            <div class="score me"> <?=$score;?> </div>
            <div style="clear:both"> </div>
            <div class="negative-bar">
                <div class="inner" style="float:right; width:<?=$score<0 ? abs($score) : "0";?>%"> </div>
            </div>
            <div class="badge" style="background-image:url('https://traitify-api.s3.amazonaws.com/traitify-api/badges/<?=strtolower($name);?>/flat/medium');"> </div>
            <div class="positive-bar">
                <div class="inner" style="float:left; width:<?=$score>0 ? $score : "0";?>%"> </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>
