<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <script src="http://cdn.traitify.com/js/api/1.0.0.js"></script>
    <script src="http://cdn.traitify.com/js/widgets/slide_deck/1.0.0.js"></script>
    <script src="http://cdn.traitify.com/js/widgets/results/totem/1.0.0.js"></script>
    <script src="http://cdn.traitify.com/js/widgets/results/prop/1.0.0.js"></script>

    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
        var assessmentID;
    </script>
</head>
<body>

    <div id="mainmenu">
        <button id="newAssessment">Create New Assessment</button>
    </div>

    <div class="assessmentArea" style="width: 600px;"></div>

    <div class="results"></div>

    <script>
        Traitify.setPublicKey("4r90k3jufseqaclg6n21bkgl2m");
        Traitify.setHost("https://api-sandbox.traitify.com");
        Traitify.setVersion("v1");

        $("#newAssessment").click(function() {
            $.ajax({
                url: 'genAssessment.php',
                method: 'POST',
                cache: false,
                data: {'ID':'<?=$_GET['userID']?>'}
            }).done(function(result) {
                assessmentID = result.assessmentID;

                $("#newAssessment").hide();

                Traitify.ui.slideDeck(assessmentID, ".assessmentArea", function(data) {
                    $(".assessmentArea").hide(); // Dunno if that happens automatically or not in the API

                    Traitify.ui.resultsProp(assessmentID, ".results", {showTraits: true});

                });

            });
        });



    </script>
</body>
</html>