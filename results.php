<?php
	$host = "hackathon.cjspy8qw09ek.us-west-2.rds.amazonaws.com";
	$port = 3306;
	$socket = "";
	$user = "hacker";
	$password = "password";
	$dbname = "hackathon";

	$mysqli = new mysqli($host, $user, $password, $dbname, $port, $socket)
	or die ('Could not connect to the database server' . mysqli_connect_error());
	$sql = "SELECT assessmentID FROM assessmentsTaken WHERE ID = " . $_GET['userID'] . ";";

	$assessmentIDs = [];

	if ($result = $mysqli->query($sql)) {
		while ($row = $result->fetch_row()) {
			$assessmentIDs[] = $row[0];
		}
		$result->close();
	}
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8"/>

	<script>var assessmentIds = [<?PHP foreach($assessmentIDs AS $k=>$aID) echo '"'.$aID.'"' . ($k < count($assessmentIDs)-1 ? ',': ''); ?>];</script>

</head>
<body>

<div class="avgResults">

</div>

<script>
	Traitify.setPublicKey("4r90k3jufseqaclg6n21bkgl2m");
	Traitify.setHost("https://api-sandbox.traitify.com");
	Traitify.setVersion("v1");
	var types = [];
	var count = 0;
	if(assessmentIds.length === 0){
		$('.avgResults')[0].innerHTML = "We don't got yo shit";
	}
	assessmentIds.forEach(function (assessmentId, index, arr) {
		Traitify.getPersonalityTypes(assessmentId, function (results) {
			count++;
			if (results.length === 0) {
				alert('no responses yet');
			} else {
				var pers_types = results.personality_types;
				pers_types.forEach(function (el, ndx, ar) {
					//for each personality type answered
					var typeName = el.personality_type.name;
					if (types[typeName] !== undefined) {
						types[typeName].score += el.score;
					} else {
						types[typeName] = el;
					}

				});
			}
			if (count === assessmentIds.length) {
				averageAssessements();
			}

		});
	})

	function averageAssessements() {
		var avgs = [];
		var c =0;
		for (var t in types){
			types[t].score = types[t].score / count;
			console.log(types[t]);
			avgs[c] = types[t];
			c++;
		}
		$.ajax({
			url: 'customResults.php',
			method: 'POST',
			cache: false,
			data: {avgCats:avgs}
		}).done(function(result) {
			$('.avgResults')[0].innerHTML = result;
		});
	}
	//Traitify.ui.resultsTotem(assessmentId, ".output", {showTraits: true});
	//    Traitify.ui.resultsProp(assessmentId, ".output", {showTraits: true});

</script>

</body>
</html>
